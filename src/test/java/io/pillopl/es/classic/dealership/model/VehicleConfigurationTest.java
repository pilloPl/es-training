package io.pillopl.es.classic.dealership.model;

import org.joda.money.Money;
import org.junit.Ignore;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.UUID;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.joda.money.CurrencyUnit.USD;
import static org.joda.money.Money.of;

public class VehicleConfigurationTest {

    static Money HUNDRED_USD = of(USD, new BigDecimal(100.00));

    VehicleConfiguration vehicleConfiguration = new VehicleConfiguration(UUID.randomUUID());

    @Test
    public void total_price_is_equals_to_sum_of_prices() {
        //given
        Item engine = Item.of("gas360hp", HUNDRED_USD);

        //when
        vehicleConfiguration.addItem(engine);

        //then
        assertThat(vehicleConfiguration.getTotalCost()).isEqualTo(HUNDRED_USD);
    }

    @Test
    public void removing_an_item_lowers_the_price() {
        //given
        Item engine = Item.of("gas360hp", HUNDRED_USD);
        //and
        vehicleConfiguration.addItem(engine);

        //when
        vehicleConfiguration.removeItem(engine);

        //then
        assertThat(vehicleConfiguration.getTotalCost()).isEqualTo(Money.zero(USD));
    }


    @Test
    public void shouldnt_be_able_to_add_more_than_15_items() {
        //given
        IntStream.rangeClosed(1, 15).forEach(
                value -> vehicleConfiguration.addItem(Item.of("i" + value, of(USD, value))));


        //expect
        Item engine = Item.of("gas360hp", HUNDRED_USD);
        assertThatExceptionOfType(TooManyItemsException.class)
                .isThrownBy(() -> vehicleConfiguration.addItem(engine));
    }

    @Test
    @Ignore
    public void should_add_10_percent_discount_after_10th_item() {
        //given
        IntStream.rangeClosed(1, 9).forEach(
                value -> vehicleConfiguration.addItem(Item.of("i" + value, HUNDRED_USD)));


        //when
        Item engine = Item.of("gas360hp", HUNDRED_USD);
        vehicleConfiguration.addItem(engine);

        //then
        assertThat(vehicleConfiguration.getTotalCost()).isEqualTo(Money.of(USD, new BigDecimal(900.00)));
    }

    @Test
    @Ignore
    public void should_remove_10_percent_discount_after_removing_10th_item() {
        //given
        IntStream.rangeClosed(1, 9).forEach(
                value -> vehicleConfiguration.addItem(Item.of("i" + value, HUNDRED_USD)));
        //and
        Item engine = Item.of("gas360hp", HUNDRED_USD);
        vehicleConfiguration.addItem(engine);

        //when
        vehicleConfiguration.removeItem(engine);

        //then
        assertThat(vehicleConfiguration.getTotalCost()).isEqualTo(Money.of(USD, new BigDecimal(900.00)));
    }

}