package io.pillopl.es.classic.dealership.model;

import lombok.Value;

@Value
class ItemCode {

    String code;
}
