package io.pillopl.es.classic.dealership.model;

import org.joda.money.Money;

import static java.math.RoundingMode.HALF_UP;

@FunctionalInterface
interface DiscountPolicy  {

    DiscountPolicy NONE = money -> money;

    DiscountPolicy TEN_PERCENT = money -> money.multipliedBy(9/10d, HALF_UP);


    Money apply(Money money);

}
