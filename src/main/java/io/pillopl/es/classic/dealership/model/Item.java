package io.pillopl.es.classic.dealership.model;

import lombok.Value;
import org.joda.money.Money;

@Value
public class Item {

    ItemCode itemCode;
    Money price;

    static Item of(String code, Money price) {
        return new Item(new ItemCode(code), price);
    }

}
