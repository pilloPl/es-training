package io.pillopl.es.classic.dealership.model;

import lombok.Getter;
import org.joda.money.Money;

import java.util.UUID;

public class VehicleConfiguration {

    @Getter private final UUID uuid;

    VehicleConfiguration(UUID uuid) {
        this.uuid = uuid;
    }

    void addItem(Item item) {
        //..
    }

    void removeItem(Item item) {
        //..
    }

    Money getTotalCost() {
        return null;
    }


}
